FROM  jupyter/tensorflow-notebook:latest

USER root

RUN conda install --quiet --yes -c conda-forge rise
RUN pip install -U altair \
                   vega_datasets \
                   notebook \
                   vega3 \
                   biopython \
                   psycopg2-binary \
                   ipython-sql  \
                   jupyter_nbextensions_configurator \
                   jupyter_contrib_nbextensions \
                   yapf \
		   tqdm \
                   pyLDAvis

# install nbextensions
RUN jupyter contrib nbextension install && jupyter nbextensions_configurator enable
RUN jupyter nbextension enable hide_input_all/main \
 && jupyter nbextension enable snippets_menu/main \
 && jupyter nbextension enable codefolding/main \
 && jupyter nbextension enable collapsible_headings/main \
 && jupyter nbextension enable hide_input/main \
 && jupyter nbextension enable toc2/main

# install jupyterlab
RUN conda install -c conda-forge jupyterlab \
&& jupyter labextension install @jupyterlab/git  \
&& pip install jupyterlab-git \
&& jupyter serverextension enable --py jupyterlab_git

RUN fix-permissions $CONDA_DIR
RUN fix-permissions /home/$NB_USER
